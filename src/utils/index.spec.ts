import * as utils from './index';

describe('utils/index.ts', () => {
  describe('objectToQueryString', () => {
    it('converts object to query string', () => {
      const source = {
        foo: 'bar',
      };

      const expected = 'foo=bar';

      expect(utils.objectToQueryString(source)).toEqual(expected);
    });
    it('convert cyrillic values to query string', () => {
      const source = {
        foo: 'Значение',
      };

      const expected = 'foo=%D0%97%D0%BD%D0%B0%D1%87%D0%B5%D0%BD%D0%B8%D0%B5';

      expect(utils.objectToQueryString(source)).toEqual(expected);
    });
  });
});
