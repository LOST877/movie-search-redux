import { RootState } from '../reducers';
import { ComponentProps } from 'react';
import InfoBlock from '../components/InfoBlock/InfoBlock';
import { connect } from 'react-redux';

type InfoBlockProps = ComponentProps<typeof InfoBlock>;

const mapStateToProps = (state: RootState): InfoBlockProps => ({
  loading: state.movies.loading,
  error: state.movies.error,
});

const InfoBlockContainer = connect(mapStateToProps)(InfoBlock);

export default InfoBlockContainer;
