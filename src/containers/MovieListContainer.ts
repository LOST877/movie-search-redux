import { ComponentProps } from 'react';
import { RootState } from '../reducers';
import { connect } from 'react-redux';
import MovieList from '../components/MovieList/MovieList';

type MovieListProps = ComponentProps<typeof MovieList>;

const mapStateToProps = (state: RootState): MovieListProps => ({
  movies: state.movies.movies,
});

const MovieListContainer = connect(mapStateToProps)(MovieList);

export default MovieListContainer;
