import { RootState } from '../reducers';
import { connect } from 'react-redux';
import TotalResults from '../components/TotalResults/TotalResults';
// reselect
const mapStateToProps = (state: RootState) => ({
  total: state.movies.movies.length,
});

const TotalResultsContainer = connect(mapStateToProps)(TotalResults);

export default TotalResultsContainer;
