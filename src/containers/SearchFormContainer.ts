import { connect } from 'react-redux';
import SearchForm from '../components/SearchForm/SearchForm';
import { searchMovies } from '../reducers/movies';

const mapDispatchToProps = (dispatch: any) => ({
  onSearch(query: string) {
    dispatch(searchMovies(query));
  },
});

/*const obj = mapDispatchToProps(() => { console.log('HI'); })
obj.onSearch('') // HI*/

const SearchFormContainer = connect(undefined, mapDispatchToProps)(SearchForm);

export default SearchFormContainer;
