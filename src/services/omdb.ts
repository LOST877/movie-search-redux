import { objectToQueryString } from '../utils';
import { MovieShort } from '../types';

const BASE_URL = 'https://www.omdbapi.com';
const API_KEY = process.env.REACT_APP_API_KEY || '';

interface OmdbSuccessResponse {
  Response: 'True';
}

interface OmdbErrorResponse {
  Response: 'False';
  Error: string;
}

const api = <T extends OmdbSuccessResponse>(
  params: Record<string, string | number | boolean> = {}
): Promise<T> => {
  const qs = objectToQueryString({ ...params, apikey: API_KEY });

  return fetch(`${BASE_URL}/?${qs}`)
    .then((response) => response.json() as Promise<T | OmdbErrorResponse>)
    .then((response) => {
      if (response.Response === 'False') {
        throw new Error(response.Error);
      }
      return response;
    });
};

interface OmdbSearchResponse extends OmdbSuccessResponse {
  Search: Array<MovieShort>;
  totalResults: string;
}

export const search = (query: string, page = 1) =>
  api<OmdbSearchResponse>({ s: query, type: 'movie', page });

interface OmdbGetByIdResponse extends OmdbSuccessResponse {}

export const getByImdbId = (imdbId: string) =>
  api<OmdbGetByIdResponse>({ i: imdbId, plot: 'full' });
