import React from 'react';
import ErrorMessage from './ErrorMessage';

export default {
  title: 'App/ErrorMessage',
  component: ErrorMessage,
  argTypes: {},
};

export const Example = () => <ErrorMessage error={new Error('Error')} />;
