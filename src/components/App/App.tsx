import Navbar from '../Navbar/Navbar';
import SearchFormContainer from '../../containers/SearchFormContainer';
import MovieListContainer from '../../containers/MovieListContainer';
import InfoBlockContainer from '../../containers/InfoBlockContainer';
import Footer from '../Footer/Footer';
import React from 'react';

const App = () => {
  return (
    <React.Fragment>
      <div className="container-fluid">
        <Navbar />
      </div>
      <main className="container">
        <div className="row py-3">
          <div className="col">
            <SearchFormContainer />
          </div>
        </div>
        <InfoBlockContainer />
        <MovieListContainer />
      </main>
      <div className="container">
        <Footer />
      </div>
    </React.Fragment>
  );
};

export default App;
