import React from 'react';

interface Props {
  total: number;
}

const TotalResults = (props: Props) => <p>Total: {props.total}</p>;

export default TotalResults;
