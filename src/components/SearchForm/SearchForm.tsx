import React from 'react';

interface Props {
  onSearch: (query: string) => void;
}

const SearchForm = (props: Props) => {
  const { onSearch } = props;
  const [query, setQuery] = React.useState('');

  const handleChange = React.useCallback((event) => {
    setQuery(event.target.value);
  }, []);

  const handleSubmit = React.useCallback(
    (event) => {
      event.preventDefault();
      onSearch(query);
    },
    [onSearch, query]
  );

  return (
    <form role="searchbox" onSubmit={handleSubmit}>
      <div className="row">
        <div className="col">
          <input
            role="search"
            name="movie-search"
            value={query}
            onChange={handleChange}
            type="search"
            placeholder="Enter movie title"
            className="form-control"
          />
        </div>
      </div>
    </form>
  );
};

export default SearchForm;
