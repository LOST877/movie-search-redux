import SearchForm from './SearchForm';
import { createEvent, fireEvent, render, screen } from '@testing-library/react';

describe('components/SearchForm', () => {
  it('handles submit event', () => {
    const callback = jest.fn();

    render(<SearchForm onSearch={callback} />);

    const form = screen.getByRole('searchbox');

    const input = screen.getByRole('search');

    const value = 'Test';

    const changeEvent = createEvent.change(input, {
      target: {
        value,
      },
    });

    fireEvent(input, changeEvent);

    const submitEvent = createEvent.submit(form);

    fireEvent(form, submitEvent);

    expect(callback).toHaveBeenCalled();
    expect(callback).toHaveBeenCalledTimes(1);
    expect(callback).toHaveBeenCalledWith(value);
  });
});
