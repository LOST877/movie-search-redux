import React from 'react';
import { MovieShort } from '../../types';

interface Props {
  movie: MovieShort;
}

const MovieCard = (props: Props) => (
  <div className="card">
    <img
      src={props.movie.Poster}
      alt={props.movie.Title}
      className="card-img-top"
    />
    <div className="card-body">
      <h5 className="card-title">{props.movie.Title}</h5>
      <p className="card-text">{props.movie.Year}</p>
      <a href={`/${props.movie.imdbID}`} className="btn btn-primary">
        View Details
      </a>
    </div>
  </div>
);

export default MovieCard;
