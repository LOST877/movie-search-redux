import React from 'react';
import Spinner from './Spinner';

export default {
  title: 'App/Spinner',
  component: Spinner,
  argTypes: {},
};

export const Example = () => <Spinner />;
