import React from 'react';
import TotalResultsContainer from '../../containers/TotalResultsContainer';

const Navbar = () => (
  <nav className="navbar navbar-light bg-light">
    <div className="container-fluid">
      <a href="/" className="navbar-brand">
        Movie Search Redux
      </a>
      <TotalResultsContainer />
    </div>
  </nav>
);

export default Navbar;
