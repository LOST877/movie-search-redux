import { combineReducers } from 'redux';
import movies, { MoviesState } from './movies';

export interface RootState {
  movies: MoviesState;
}

const rootReducer = combineReducers<RootState>({
  movies: movies,
  // movieById: movieById
});

export default rootReducer;
