import { MovieShort } from '../types';
import { search } from '../services/omdb';

export interface MoviesState {
  loading: boolean;
  error: Error | null;
  movies: Array<MovieShort>;
}

const defaultState: MoviesState = {
  loading: false,
  error: null,
  movies: [],
};

export const setLoading = (value: boolean) =>
  ({
    type: 'SET_LOADING',
    payload: value,
  } as const);

export const setError = (value: Error) =>
  ({
    type: 'SET_ERROR',
    payload: value,
  } as const);

export const setMovies = (value: MovieShort[]) =>
  ({
    type: 'SET_MOVIES',
    payload: value,
  } as const);

export const searchMovies = (query: string) => (dispatch: any) => {
  dispatch(setLoading(true));
  return search(query)
    .then((response) => {
      dispatch(setMovies(response.Search));
    })
    .catch((error) => {
      dispatch(setError(error));
    });
};

type Action = ReturnType<
  typeof setLoading | typeof setError | typeof setMovies
>;

const reducer = (state = defaultState, action: Action) => {
  switch (action.type) {
    case 'SET_LOADING':
      return {
        ...state,
        movies: [],
        loading: action.payload,
      };
    case 'SET_ERROR':
      return {
        ...state,
        movies: [],
        loading: false,
        error: action.payload,
      };
    case 'SET_MOVIES':
      return {
        ...state,
        movies: action.payload,
        loading: false,
        error: null,
      };
    default:
      return state;
  }
};

export default reducer;
