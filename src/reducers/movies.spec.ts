import reducer, { MoviesState, setError, setLoading } from './movies';

describe('reducers/movies.ts', () => {
  it('handles setLoading action', () => {
    const state: MoviesState = {
      loading: false,
      movies: [],
      error: null,
    };

    const action = setLoading(true);

    expect(reducer(state, action)).toEqual({
      ...state,
      loading: true,
    });
  });
  it('handles setError action', () => {
    const state: MoviesState = {
      loading: true,
      error: null,
      movies: [],
    };

    const error = new Error('test');

    const action = setError(error);

    expect(reducer(state, action)).toEqual({
      loading: false,
      error: error,
      movies: [],
    });
  });
});
