export interface MovieShort {
  Poster: string;
  Title: string;
  Type: string;
  Year: string;
  imdbID: string;
}

export interface Movie extends MovieShort {
  Plot: string;
}
